yafeya.braceScope = yafeya.braceScope || {}; // means to define a namespace.

(function () {
    function es5DeclarationForBrace() {
        var condition = true;
        if (condition) {
            var varNumber = 999;
        }
        console.log(varNumber);
    }

    function es6DeclarationForBrace() {
        let condition = true;
        if (condition) {
            let letNumber = 999; // letNumber cannot be access outside
        }
        console.log(letNumber);
    }

    try {
        es5DeclarationForBrace();
        es6DeclarationForBrace();
    } catch (error) {
        console.log(error);
    }
}).call(yafeya.braceScope);