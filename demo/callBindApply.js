yafeya.callBindApply = yafeya.callBindApply || {};

(function () {
    var user = {
        user_name: "yafeya",
        speak: function () {
            console.log(this.user_name);
            user.speak(); // yafeya
            var speak = user.speak;
            var hello = user.hello;
            speak(); // undefined, since variable fn is a anonymous method, the this inside will point to window object.

            console.log('call & apply');
            speak.call(user); // yafeya, call will make this inside speak function point to user.
            speak.apply(user); // yafeya, apply will make this inside speak function point to user.

            console.log('call & apply with parameters');
            hello.call(user, 'hello'); // call method's parameter list could be flat or array, you can just add them like (object, parameter1, parameter2, ...)
            hello.apply(user, ['hello']); // apply method's parameter list must be an array, you must add them like (object, [parameter1, parameter2, ...]), otherwise it will throw an exception.

            console.log('bind demo')
            hello.bind(user, 'hello')(); // yafeya, bind will make this inside speak function point to user, but the bind return a function, need to call it.
        },
        hello: function (word) {
            console.log(`${this.user_name} said ${word}`);
        }
    }
}).call(yafeya.callBindApply);
