yafeya.callbackQueueDemo = yafeya.callbackQueueDemo || {};

(function () {
    function display(text) {
        console.log(text);
    }

    display(`This is the 1st time to call display()...`);
    setTimeout(function () {
        display(`This is the 2nd time to call display()...`);
    }, 0);
    display(`This is the 3rd time to call display()...`);
}).call(yafeya.callbackQueueDemo);