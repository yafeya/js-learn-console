yafeya.closure = yafeya.closure || {};

(function () { 
    function closureVariableTest1() {
        var result = new Array();
        for (var i = 0; i < 10; i++) {
            result[i] = function () {
                return i;
            };
        }
        return result;
    }
    
    function closureVariableTest2() {
        var result = new Array();
        for (var i = 0; i < 10; i++) {
            result[i] = (function () {
                var num = i;
                return num;
            })(i)
        }
        return result;
    }

    var result1 = closureVariableTest1();
    var result2 = closureVariableTest2();

    for (var i = 0; i < result1.length; i++) {
        console.log(result1[i]()); // 10, 10, 10, 10, 10, 10, 10, 10, 10, 10
        /**
         * Since there is no brace scope for var, so, result1's element will store the function,
         * and when the function executed, the i is always 10.
         */
    }

    for (var i = 0; i < result2.length; i++) {
        console.log(result2[i]); // 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
        /**
         * Every element store the execute result of the function, so it is expected.
         */
    }
}).call(yafeya.closure);