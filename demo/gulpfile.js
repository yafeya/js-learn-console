var gulp = require('gulp');
var merge = require('merge-stream');
var order = require('gulp-order');
var del = require('del');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('clean', function () {
    return del(['bundle.js']);
});

gulp.task('bundle', function () {
    return  merge(gulp.src(['./init.js', './inherit-traditional.js'])//  merge(gulp.src(['./*.js', '!./gulpfile.js'])
        .pipe(sourcemaps.init())
        .pipe(order(['init.js', '*.js']))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(concat('bundle.js'))
        .pipe(sourcemaps.write({ includeContent: false, sourceRoot: './' })) // enable debugging
        .pipe(gulp.dest('./')));
});

exports.build = gulp.series('clean', 'bundle');