function Person(name, age) {
    this.name = name;
    this.age = age;

    this.say = function () {
        console.log(`${this.name}: ${this.age}`);
    }
}

function Engineer(name, age, title) {
    Person.call(this, name, age);

    this.title = title;

    this.say = function () {
        console.log(`${this.name}(${this.title}): ${this.age}`);
    }
}

Engineer.prototype = new Person(); // sencond try.

var person1 = new Person('person1', 1);
person1.say(); // person1: 1

var engineer1 = new Engineer('engineer', 2, 'advanced');
engineer1.say();

console.log(`engineer inherit from Person: ${engineer1 instanceof Person}`);
console.log(`person inherit from Engineer: ${person1 instanceof Engineer}`);