yafeya.inheritDemo = yafeya.inheritDemo || {};

(function () {
    function Job() {
        this.name = "No Job";
    }

    function Person(name, age) {
        this.job = new Job();
        this.name = name;
        this.age = age;
    };

    Person.prototype.introduce = function () {
        console.log(`${this.name}, ${this.age}`);
    }

    function Engineer(name, age) {
        // Standard way to call base-class constructor.
        // Otherwise, the reference property will be shared by child-class.
        // e.g. remove this line, when you change the job name, all the instance of job.name will be the same.
        Person.call(this, name, age);
    };

    Engineer.prototype = new Person(this.name, this.age);
    Engineer.prototype.title = "Principle";

    Engineer.prototype.introduce = function () {
        console.log(`${this.name}, ${this.age}, ${this.job.name}, ${this.title}`);
    };

    function inheritDemo() {
        var person1 = new Person("frank", 36);
        var person2 = new Person("mario", 1.5);

        person1.introduce();
        person2.introduce();

        var engineer1 = new Engineer("feiyang", 36);
        engineer1.job.name = "Software Engineer";
        engineer1.title = "Principle Software Engineer";

        var engineer2 = new Engineer("mario yang", 1.5);
        engineer2.job.name = "baby";
        engineer2.title = "new baby";

        engineer1.introduce();
        engineer2.introduce();

        console.log(`person1 instanceof Person? ${person1 instanceof Person}`);
        console.log(`person1 instanceof Engineer? ${person1 instanceof Engineer}`);
        console.log(`engineer1 instanceof Person? ${engineer1 instanceof Person}`);
        console.log(`engineer1 instanceof Engineer? ${engineer1 instanceof Engineer}`);

        console.log(`Person.introduce is own? ${person1.hasOwnProperty("introduce")}`);
        console.log(`Engineer.introduce is own? ${engineer1.hasOwnProperty("introduce")}`);
        console.log(`Engineer.name is own? ${engineer1.hasOwnProperty("name")}`);
    }

    inheritDemo();
}).call(yafeya.inheritDemo);

