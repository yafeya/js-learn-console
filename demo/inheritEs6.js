class Job {
    _name = '';

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }
}

class Person {
    _name = '';
    _age = 0;
    _job = undefined;

    constructor(name, age) {
        this._name = name;
        this._age = age;
        this._job = new Job();
    }

    get name() {
        return this._name;
    }

    get job() {
        return this._job;
    }

    get age() {
        return this._age;
    }

    set name(value) {
        this._name = value;
    }

    set age(value) {
        this._age = value;
    }

    introduce = function () {
        console.log(`${this._name}: ${this._age}`);
    }
}

class Engineer extends Person {
    _title = '';

    constructor(name, age, title) {
        super(name, age);
        this._title = title;
    }

    get title() {
        return this._title;
    }

    set title(value) {
        this._title = value;
    }

    introduce = function () {
        console.log(`${this._name} (${this._title}): ${this._age}`);
    }
}

var person1 = new Person("frank", 36);
var person2 = new Person("mario", 1.5);

person1.introduce();
person2.introduce();

var engineer1 = new Engineer("feiyang", 36);
engineer1.job.name = "Software Engineer";
engineer1.title = "Principle Software Engineer";

var engineer2 = new Engineer("mario yang", 1.5);
engineer2.job.name = "baby";
engineer2.title = "new baby";

engineer1.introduce();
engineer2.introduce();

console.log(`person1 instanceof Person? ${person1 instanceof Person}`);
console.log(`person1 instanceof Engineer? ${person1 instanceof Engineer}`);
console.log(`engineer1 instanceof Person? ${engineer1 instanceof Person}`);
console.log(`engineer1 instanceof Engineer? ${engineer1 instanceof Engineer}`);

console.log(`Person.introduce is own? ${person1.hasOwnProperty("introduce")}`);
console.log(`Engineer.introduce is own? ${engineer1.hasOwnProperty("introduce")}`);
console.log(`Engineer.name is own? ${engineer1.hasOwnProperty("name")}`);
