yafeya.promiseDemo = yafeya.promiseDemo || {};

(function () {
    function simpleExample(text) {
        return new Promise((resolve, reject) => {
            if (text == undefined || text === '')
                reject('error');
            console.log(text);
            resolve('done');
        });
    }

    var result1 = simpleExample();
    var result2 = simpleExample('hello');
    result1.then(value => console.log(value)).catch(err => console.log(err));
    result2.then(value => console.log(value)).catch(err => console.log(err));

}).call(yafeya.promiseDemo);