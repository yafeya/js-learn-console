yafeya.promiseEventLoop = yafeya.promiseEventLoop || {};

(function () { 
    setTimeout(function(){
        console.log(`promise eventloop: ${1}`);
    });
    
    var promise = new Promise(function(resolve){
        console.log(`promise eventloop: ${2}`);
        resolve();
    });
    
    promise.then(function(){
        console.log(`promise eventloop: ${3}`);
    });
    
    console.log(`promise eventloop: ${4}`);
}).call(yafeya.promiseEventLoop);