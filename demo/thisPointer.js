yafeya.thisPointer = yafeya.thisPointer || {};

(function () {
    function useThisInsideObject() {
        try {
            window.name = 'The Window';
            var object = {
                name: 'My Object',
                getNameFunc: function () {
                    return function () {
                        return this.name;
                    }
                }
            };

            console.log(object.getNameFunc()());
            /**
             * non-strict: `The Window`, strict: undefined, since object.getNameFunc()() will call the anonymous method inside the getNameFunc.
             * non-strict: all the this object inside anonymouse method will point to window.
             * strict: all the this object inside anonymouse method will be undefined.
            */
        } catch (error) {
            console.log(error);
        }
    }

    function keepTheContextOfObject() {
        try {
            name = 'The Window';
            var object = {
                name: 'My Object',
                getNameFunc: function () {
                    var self = this;
                    return function () {
                        return self.name;
                    }
                }
            };

            console.log(object.getNameFunc()());
            /**
             * `My Object`, since before call the anonymouse method, have documented the state of this, in getNameFunction(), this will point to `object`.
             * So, it will output the 'My Object'
            */
        } catch (error) {
            console.log(error);
        }
    }

    useThisInsideObject();
    keepTheContextOfObject();
}).call(yafeya.thisPointer);