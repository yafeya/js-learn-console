yafeya.variableDeclaration = yafeya.variableDeclaration || {};

(function () {
    function varDeclaration() {
        var number1 = 999;

        function display() {
            console.log(number1); // number1 is accessible in `display` method
        }

        return display;
    }

    function globalDeclaration() {
        function defineNumber() {
            try {
                number2 = 999; // this will define a global object global.globalNumber;    
            } catch (error) {
                console.log(error);
            }
            
        }
        return defineNumber;
    }

    function internalVariableDeclaration() {
        function defineNumber() {
            var number3 = 999; // number3 cannot be access outside.
        }
        return defineNumber;
    }

    try {
        varDeclaration()();
        globalDeclaration()();
        console.log(window.number2);
        internalVariableDeclaration()();
        console.log(nubmer3);
    } catch (err) {
        console.log(err);
    }
}).call(yafeya.variableDeclaration);